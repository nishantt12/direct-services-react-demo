import React from 'react';

import Alert from 'react-s-alert';
 
import '../../../node_modules/react-s-alert/dist/s-alert-default.css';
import '../../../node_modules/react-s-alert/dist/s-alert-css-effects/slide.css';



export default ({children}) => {
  return (
    <div id="container">
      {children}
      <Alert stack={{limit: 3}} />
    </div>
  );
}
