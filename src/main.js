/**
 * App entry point
 */

// Polyfill
import 'babel-polyfill';

// Libraries
import React from 'react';
import ReactDOM from 'react-dom';
import { Router, browserHistory } from 'react-router';

// Routes
import Routes from './common/components/Routes';
import injectTapEventPlugin from 'react-tap-event-plugin';

import Alert from 'react-s-alert';
 
import '../node_modules/react-s-alert/dist/s-alert-default.css';
import '../node_modules/react-s-alert/dist/s-alert-css-effects/slide.css';
 



// Base styling
import './common/base.css';


// ID of the DOM element to mount app on
const DOM_APP_EL_ID = 'app';

injectTapEventPlugin();


// Render the router
ReactDOM.render((
  <Router history={browserHistory}>
    {Routes}
                    <Alert stack={{limit: 3}} />

  </Router>

), document.getElementById(DOM_APP_EL_ID));

