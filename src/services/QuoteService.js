import request from 'reqwest';

import {QUOTE_URL} from '../constants/QuoteConstants';
import QuoteActions from '../actions/QuoteActions';


class QuoteService {


  nextQuote() {
    console.log('nextQuote called')
    request({
      url: 'http://localhost:8080/api/clients',
      method: 'GET',
      crossOrigin: true,
      headers: {
        'Authorization': 'Bearer ' 
      }
    })
    .then(function(response) {
      console.log('In Service to get data, data received on api hit', JSON.stringify(response))
      QuoteActions.gotQuote(response);
      return response
    });
  }

}

export default new QuoteService()
