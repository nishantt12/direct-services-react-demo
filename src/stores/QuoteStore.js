import {QUOTE_GET} from '../constants/QuoteConstants';

import BaseStore from './BaseStore';

class QuoteStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(() => this._registerToActions.bind(this))
    this._quote = ' ';
  }

  _registerToActions(action) {
    console.log('In store Service this is action');

    this._quote = action;
    this.emitChange();
    
  }

  get quote() {
    console.log('This is q', this._quote)
    return this._quote;
  }
}

export default new QuoteStore();
