var BASE_URL = 'http://localhost:8080/';
export default {
  BASE_URL: BASE_URL,
  QUOTE_URL: BASE_URL + 'api/clients',
  QUOTE_GET: 'QUOTE_GET'
}
