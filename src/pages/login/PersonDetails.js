import React from 'react';

import styles from './style.css';

import ReactMixin from 'react-mixin';


import '../../../node_modules/react-s-alert/dist/s-alert-default.css';
import '../../../node_modules/react-s-alert/dist/s-alert-css-effects/slide.css';
import addons from 'react-addons';



export default class CardMain extends React.Component {

    constructor() {
        super()
        this.state = {

        };
    }

    render() {
        var name = this.props.value;
        console.log("Render called", this.props.value)

        if (name) {
            return (
                <div className={ styles.white}>
                    <div className={styles.imageDiv}>
                        <img className={styles.full_image} src={name.general.avatar} alt="Mountain View" align="left"/>
                    </div>
                    <div className= {styles.detail}>
                        <h1>{name.general.firstName} {name.general.lastName}</h1>
                        <h2>Job</h2>
                        <p><strong>Company:</strong> {name.job.company}</p>
                        <p><strong>Title:</strong> {name.job.title}</p>
                        <h2>Contact</h2>
                        <p><strong>Email:</strong> {name.contact.email}</p>
                        <p><strong>Phone:</strong> {name.contact.phone}</p>
                        <h2>Address</h2>
                        <p><strong>Street:</strong> {name.address.street}</p>
                        <p><strong>City:</strong> {name.address.city}</p>
                        <p><strong>Zip Code:</strong> {name.address.zipCode}</p>
                        <p><strong>Country:</strong> {name.address.country}</p>
                    </div>
                </div>
            );
        }
        else {
            return (
                <div className={ styles.white}></div>
            );
        }
    }
}
ReactMixin(CardMain.prototype, addons.LinkedStateMixin);
