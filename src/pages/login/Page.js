import React from 'react';

import styles from './style.css';

import ReactMixin from 'react-mixin';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


import QuoteStore from '../../stores/QuoteStore.js';
import QuoteService from '../../services/QuoteService.js';

import '../../../node_modules/react-s-alert/dist/s-alert-default.css';
import '../../../node_modules/react-s-alert/dist/s-alert-css-effects/slide.css';

var addons = require('react-addons');

import Avatar from 'material-ui/Avatar';
import {List, ListItem} from 'material-ui/List';
import PersonDetails from './PersonDetails';

import SearchInput, {createFilter} from 'react-search-input'


const KEYS_TO_FILTERS = ['general.firstName', 'general.lastName', 'job.company', 'job.title', 'contact.email', 'contact.phone', 'address.street', 'address.city', 'address.zipCode', 'address.country']

export default class LoginPage extends React.Component {


    constructor() {
        super()
        this.state = {
            value: '',
            searchTerm: '',
            names: [],
            selected: ''

        };

    }

    componentDidMount() {
        console.log('componentWillMount called !')

        this.requestNextQuote();

        QuoteStore.addChangeListener(this._onChange.bind(this));
    }

    componentWillUnmount() {
        QuoteStore.removeChangeListener(this._onChange.bind(this));
    }

    componentDidUpdate() {
        console.log('Component did update called');
    }

    _onChange() {
        var objectData = QuoteStore.quote;

        console.log(objectData);

        this.setState({
            names: objectData.quote,
            selected: objectData.quote[0]
        });
        console.log('Selected state - ', JSON.stringify(this.state.selected));
    }

    requestNextQuote() {
        QuoteService.nextQuote();
    }


    handleFilterChange(value) {

        console.log('This is the value -', value)
        this.setState({
            selected: value
        });
    }

    render() {

        console.log(this.state.searchTerm);
        const filteredEmails = this.state.names.filter(createFilter(this.state.searchTerm, KEYS_TO_FILTERS))

        return (
            <div>
                <div id="container">
                    <div className={styles.serachdiv}>
                        <MuiThemeProvider>
                            <List>
                                <div className={styles.margin}>
                                    <SearchInput className="search-input" onChange={this.searchUpdated.bind(this)}/>
                                </div>
                                {this.createItems(filteredEmails)}
                            </List>
                        </MuiThemeProvider>
                    </div>

                </div>
                <div className={styles.maincontent}>
                    <div className={styles.maincontent2}>
                        <PersonDetails value={this.state.selected}/>
                    </div>
                </div>
            </div>
        );
    }

    searchUpdated(term) {
        this.setState({searchTerm: term})
    }

    createItems(items) {

        if (items.length > 0) {
            var output = [];
            for (var i = 0; i < items.length; i++) {

                var name = items[i];

                output.push(<div onClick={this.handleFilterChange.bind(this,name)}>
                        <ListItem key={i} primaryText={name.general.firstName} secondaryText={name.job.title}
                                  leftAvatar={<Avatar className={styles.resize_fit_center} src={name.general.avatar}/>}
                        />
                    </div>
                );
            }
            return output;
        }
    }

}
ReactMixin(LoginPage.prototype, addons.LinkedStateMixin);
