# Solution of the React.js interview task for Direct Services.

(Used the essential-react starter kit to setup the tools)

Uses: React, Webpack, Babel, Bootstrap, Material-UI


### To run: 

```sh
$ npm install
```

Start the local dev server:

```sh
$ npm run server
```

Navigate to **http://localhost:8080/** to view the app.


**Input:** `src/main.jsx`


This leverages [React Hot Loader](https://github.com/gaearon/react-hot-loader) to automatically start a local dev server and refresh file changes on the fly without reloading the page.


### build

```sh
$ npm run build
```

**Input:** `src/main.jsx`

**Output:** `build/app.js`

Build minified app for production using the [production](http://webpack.github.io/docs/cli.html#production-shortcut-p) shortcut.


### test

```sh
$ npm test
```

**Input:** `test/main.js`

**Output:** `coverage/`

Leverages [ava](https://github.com/sindresorhus/ava) to execute the test suite and generate code coverage reports using [nyc](https://github.com/bcoe/nyc)


### clean

```sh
$ npm run clean
```

**Input:** `build/app.js`

Removes the compiled app file from build.